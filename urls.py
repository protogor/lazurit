"""lazurit URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
import settings

from django.conf.urls import url, include
from django.contrib import admin

from django.views.static import serve as static_view
from apps.catalog.urls import urlpatterns as catalog_url
from apps.articles.urls import urlpatterns as articles_url
from apps.basket.urls import urlpatterns as basket_url
from apps.contacts.urls import urlpatterns as contacts_url
from apps.seo.urls import urlpatterns as seo_url
from apps.marketing.urls import urlpatterns as marketing_url


urlpatterns = [
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^media/(?P<path>.*)$', static_view, {'document_root': settings.MEDIA_ROOT}),
    url(r'^static/(?P<path>.*)$', static_view, {'document_root': settings.STATIC_ROOT})
]

urlpatterns += marketing_url
urlpatterns += seo_url
urlpatterns += contacts_url
urlpatterns += basket_url
urlpatterns += articles_url
urlpatterns += catalog_url

from django.db import models
from apps.catalog.models import Product
from apps.marketing.models import Subscriber
from django.db.models import Sum
# Create your models here.

ORDER_NEW = "n"
ORDER_SIGN = "s"
ORDER_READY = "r"
ORDER_CANCEL = "c"

ORDER_STATUS = (
    (ORDER_NEW, "Новый"),
    (ORDER_SIGN, "Зарегистрирован"),
    (ORDER_READY, "Выполнен"),
    (ORDER_CANCEL, "Отменен"),
)

PAIDS = (('pac','Оплата по счету'),
        ('pdl','Оплата при доставке'),
        ('pcd','Банковская карта'))


DELIVERIES = (('dslf','Самовывоз'),
        ('dspb','Доставка по СПб'),
        ('ddtk','Доставка до ТК'))


class Basket(models.Model):
    session = models.CharField(max_length=32, db_index=True)
    name = models.CharField("Имя", max_length=150, blank=True)
    address = models.CharField("Адрес", max_length=150, blank=True)
    city = models.CharField("Город", max_length=150, blank=True)
    telephone = models.CharField("Телефон", max_length=150, blank=True)
    email = models.EmailField("Почта", blank=True)
    comment = models.TextField("Комментарий", blank=True)
    paid = models.CharField("Тип платежа", choices=PAIDS, default="pac", max_length=3)
    delivery = models.CharField("Тип доставки", choices=DELIVERIES, default="dslf", max_length=4)
    status = models.CharField("Статус", choices=ORDER_STATUS, max_length=1, default=ORDER_NEW)
    create = models.DateTimeField("Время создания", auto_now_add=True)
    modified = models.DateTimeField(
        verbose_name=u'Время последнего изменения', auto_now=True)

    def save(self, *args, **kwargs):
        if self.email:
            Subscriber.objects.get_or_create(email=self.email)
        super(Basket, self).save(*args, **kwargs)

    @property 
    def sum(self):
        amount = 0
        for item in self.items.all():
            amount += item.sum
        return amount

    @property 
    def count(self):
        count = self.items.aggregate(Sum("quantity")).get("quantity__sum", 0)
        if count:
            return count
        return 0

    def get_paid_name(self):
        paids_type = dict(PAIDS)
        return paids_type.get(self.paid)

    def get_delivery_name(self):
        delivery_type = dict(DELIVERIES)
        return delivery_type.get(self.delivery)

    class Meta:
        verbose_name = "Корзина"
        verbose_name_plural = "Корзины"


class BasketItem(models.Model):
    basket = models.ForeignKey("Basket", verbose_name="Заказанный товар", related_name="items")
    product = models.ForeignKey(Product, verbose_name="Продукт", related_name="bitems")
    quantity = models.IntegerField(verbose_name="Количество", default=1)
    price = models.IntegerField(verbose_name="Цена", default=0)
    create = models.DateTimeField("Время создания", auto_now_add=True)

    class Meta:
        verbose_name = "Заказ"
        verbose_name_plural = "Заказы"
     
    @property 
    def sum(self):
        return self.price * self.quantity




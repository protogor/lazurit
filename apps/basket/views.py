from apps.catalog.models import Product
from apps.basket.models import Basket, BasketItem, ORDER_NEW, ORDER_SIGN
from apps.basket.forms import OrderForm
from apps.catalog.templatetags.common_tags import price_format
from django.template.loader import render_to_string
from apps.market.models import Market, SendEmail, POST_ORDER, POST_FEED_BACK, POST_FORM

from django.http import JsonResponse, Http404
from django.shortcuts import render
from django.db.models import Q, F

from apps.core.utils import Email, BrokerThread


# Create your views here.

class EmailOrder(Email):
    pass

# Create your views here.

def get_basket(request):
    session_key = request.session.session_key 
    if not session_key:
        request.session.flush()
        request.session.save()
        session_key = request.session.session_key 

    basket = Basket.objects.get_or_create(session=session_key, status=ORDER_NEW)[0]
    return basket


def cart(request):
    
    basket = get_basket(request)
    return render(request, "basket/cart.html", {"cart": basket})


def order(request):

    basket = get_basket(request)
    if request.method == "POST":
        form = OrderForm(request.POST)
        if form.is_valid():
            basket.name = form.cleaned_data["fio"]
            #basket.address = form.cleaned_data["address"]
            #basket.city = form.cleaned_data["city"]
            basket.telephone = form.cleaned_data["telephone"]
            basket.email = form.cleaned_data["email"]
            basket.comment = form.cleaned_data["comment"]
            basket.paid = form.cleaned_data["paids"]
            basket.delivery = form.cleaned_data["delivery"]
            basket.status = ORDER_SIGN
            basket.save()


            emails_send = SendEmail.objects.prefetch_related("emails").\
                        filter(active=True, type_mail=POST_ORDER, emails__active=True).\
                        values_list("emails__email", flat=True)
            if emails_send:
                    
                title = "Заказ Лазурит принт"
                email_template = render_to_string("emails/email_order_personal.html", {
                    "basket": basket
                })

                email_order = EmailOrder(title, email_template, list(emails_send))
                email_order.execute()
                """
                broker = BrokerThread()
                broker.add(email_order)
                broker.run()
                """


            #window_order = render_to_string("window/window_order_ok.html", {"basket": basket})
            return JsonResponse({"status": "ok", "order_id": basket.id})

            #request.session.flush()
            #request.session.save()
        return JsonResponse({
                "status": "error", 
                "errors": form.errors
            })

    form = OrderForm()


    return render(request, "basket/order.html", {
            "cart": basket, 
            "form": form
        })


def cart_add(request):
    product_id = request.POST.get("product_id")
    try:
        product = Product.objects.get(id=product_id)
    except:
        raise Http404

    count = request.POST.get("count", 1)
    try:
        count = max(int(count), 1)
    except Exception:
        raise Http404


    basket = get_basket(request)
    if not basket.items.filter(product=product).update(quantity=F("quantity") + count):
        basket.items.create(
            product=product,
            price=product.price,
            quantity=count
        )

    return JsonResponse({
            "status": "ok", 
            "count": basket.count, 
            "sum": price_format(basket.sum)
        })
    """
    basket_html = render_to_string("tags/basket.html", {"basket": basket})
    return JsonResponse({"basket": basket_html})

    return JsonResponse({"product_id": product_id})
    """

def cart_delete(request):
    item_id = request.POST.get("item_id")
    try:
        item = BasketItem.objects.get(id=item_id)
    except:
        raise Http404


    basket = get_basket(request)
    basket.items.filter(id=item.id).delete()

    return JsonResponse({
            "status": "ok", 
            "count": basket.count, 
            "sum": price_format(basket.sum)
        })


def cart_change(request):
    item_id = request.POST.get("item_id")
    #try:
    item = BasketItem.objects.get(id=item_id)
    #except:
    #    raise Http404

    count = request.POST.get("count", 1)
    try:
        count = max(int(count), 1)
    except Exception:
        raise Http404


    basket = get_basket(request)
    b_items = basket.items.filter(id=item.id)
    b_items.update(quantity=count)

    return JsonResponse({
            "status": "ok", 
            "count": basket.count, 
            "sum": price_format(basket.sum),
            "item_sum": price_format(b_items[0].sum)
        })
from django import forms


PAIDS = (('pac','Оплата по счету'),
        ('pdl','Оплата при доставке'),
        ('pcd','Банковская карта'))


DELIVERIES = (('dslf','Самовывоз'),
        ('dspb','Доставка по СПб'),
        ('ddtk','Доставка до ТК'))


class OrderForm(forms.Form):
    fio = forms.CharField(label="ФИО", max_length=150, widget=forms.TextInput({"class": "lp_form-input"}))
    #address = forms.CharField(label="Адрес", max_length=150, widget=forms.TextInput({"class": "lp_form-input"}))
    #city = forms.CharField(label="Населенный пункт", max_length=150, widget=forms.TextInput({"class": "lp_form-input"}))
    fio = forms.CharField(label="ФИО", max_length=150, widget=forms.TextInput({"class": "lp_form-input"}))
    telephone = forms.CharField(label="Телефон", max_length=150, widget=forms.TextInput({"class": "lp_form-input"}))
    email = forms.CharField(label="E-mail", max_length=150, widget=forms.TextInput({"class": "lp_form-input"}))
    comment = forms.CharField(label="Телефон", widget=forms.Textarea({"class": "lp_form-input"}), required=False)
    paids = forms.ChoiceField(choices=PAIDS, widget=forms.RadioSelect())
    delivery = forms.ChoiceField(widget=forms.Select({"class": "lp_form-input"}), choices=DELIVERIES)
from django.contrib import admin
from apps.basket.models import Basket, BasketItem


class BasketItemInline(admin.TabularInline):
    model = BasketItem


class BasketAdmin(admin.ModelAdmin):
    list_display = ("id", "session", "status", "create", "modified")
    list_display_links = ("session",)
    inlines = [
    	BasketItemInline
    ]

    
admin.site.register(Basket, BasketAdmin)
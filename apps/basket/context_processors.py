from apps.basket.models import Basket, ORDER_NEW
from apps.basket.views import get_basket


def basket(request):
    count, sum = 0, 0

    try:
        basket = get_basket(request)
        count = basket.count
        sum = basket.sum
    except Exception:
        pass

    return {
        "count": count, 
        "sum": sum
    }

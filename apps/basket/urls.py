from django.conf.urls import url, include

from . import views


urlpatterns = [
    url(r'^cart/$', views.cart),
    url(r'^cart/add/$', views.cart_add),
    url(r'^cart/delete/$', views.cart_delete),
    url(r'^cart/change/$', views.cart_change),
    url(r'^order/$', views.order),
]

from django.contrib import admin
from .models import Contacts


class ContactsAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "email", "theme", "message", "create")
    list_display_links = ("id",)

    
admin.site.register(Contacts, ContactsAdmin)

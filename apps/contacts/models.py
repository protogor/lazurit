from django.db import models

# Create your models here.

class Contacts(models.Model):
    name = models.CharField("Имя", max_length=150)
    email = models.EmailField("Почта", blank=True)
    theme = models.CharField("Тема", max_length=150)
    message = models.TextField("Сообщение")
    create = models.DateTimeField("Время создания", auto_now_add=True)

    class Meta:
        verbose_name = "Контакт"
        verbose_name_plural = "Контакты"

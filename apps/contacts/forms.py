from django import forms


class ContactForm(forms.Form):
    name = forms.CharField(label="Имя", max_length=150, widget=forms.TextInput({"class": "lp_form-input"}))
    email = forms.CharField(label="E-mail", max_length=150, widget=forms.TextInput({"class": "lp_form-input"}))
    theme = forms.CharField(label="Тема", max_length=150, widget=forms.TextInput({"class": "lp_form-input"}))
    message = forms.CharField(label="Сообщение", widget=forms.Textarea({"class": "lp_form-input"}))

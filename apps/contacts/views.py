from django.shortcuts import render
from apps.contacts.models import Contacts
from apps.contacts.forms import ContactForm

from django.http import JsonResponse, Http404
# Create your views here.

def contact(request):
    if request.method == "POST":
        form = ContactForm(request.POST)
        if form.is_valid():
            contact = Contacts()
            contact.name = form.cleaned_data["name"]
            contact.email = form.cleaned_data["email"]
            contact.theme = form.cleaned_data["theme"]
            contact.message = form.cleaned_data["message"]
            contact.save()
            return JsonResponse({"status": "ok"})
        return JsonResponse({
                "status": "error", 
                "errors": form.errors
            })

    form = ContactForm()
    return render(request, "page/contact.html", {"form": form})

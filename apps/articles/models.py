from django.db import models
from pytils.translit import slugify

from ckeditor.fields import RichTextField

# Create your models here.
class Article(models.Model):
    category = models.ForeignKey("Category", related_name="articles", blank=True, null=True)
    name = models.CharField("Название", max_length=256)
    slug = models.CharField("slug", max_length=256, db_index=True, blank=True)
    image = models.ImageField("Картинка", upload_to="images/articles/", blank=True, null=True)
    text = RichTextField("Статья")
 
    active = models.BooleanField("Активный", default=True)
    order = models.SmallIntegerField("Очередность вывода", default=10)
    modified = models.DateTimeField('Время последнего изменения', auto_now=True)
    created = models.DateTimeField('Время создания', auto_now_add=True)

    @property
    def url(self):
        return "/page/{}/".format(self.slug)

    def save(self, *args, **kwargs):        
        if not self.slug:
            self.slug = slugify(self.name)
        return super(Article, self).save(*args, **kwargs)

    class Meta:
        verbose_name = "Статья"
        verbose_name_plural = "Статьи"


class Category(models.Model):
    name = models.CharField("Название", max_length=256)
    slug = models.CharField("slug", max_length=256, db_index=True)
    image = models.ImageField("Картинка", upload_to="images/categories/", blank=True, null=True)
    
    active = models.BooleanField("Активный", default=True)
    order = models.SmallIntegerField("Очередность вывода", default=10)
    modified = models.DateTimeField('Время последнего изменения', auto_now=True)
    created = models.DateTimeField('Время создания', auto_now_add=True)

    def __str__(self):
        return self.name

    @property
    def url(self):
        return "/{}/".format(self.slug)

    def save(self, *args, **kwargs):        
        if not self.slug:
            self.slug = slugify(self.name)
        return super(Category, self).save(*args, **kwargs)

    class Meta:
        verbose_name = "Категрия"
        verbose_name_plural = "Категрии"
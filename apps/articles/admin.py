from django.contrib import admin
from apps.articles.models import Article, Category
# Register your models here.

class ArticleAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "created")
    list_display_links = ("name",)

    class Media:
        js = ('ckeditor.js',)
    
admin.site.register(Article, ArticleAdmin)


class CategoryAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "created")
    list_display_links = ("name",)
    
admin.site.register(Category, CategoryAdmin)
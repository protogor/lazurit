from django.shortcuts import render
from django.http import Http404

from apps.articles.models import Article, Category


def articles(request):
    category = Category.objects.get(id=1)
    articles = category.articles.filter(active=True)
    return render(request, "articles/articles.html", {
        "category": category,
        "articles": articles
    })

def article(request, slug):
    try:
        article = Article.objects.get(slug=slug)
    except:
        raise Http404
    return render(request, "articles/article.html", {"article": article})


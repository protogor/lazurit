from django.conf.urls import url, include

from . import views


urlpatterns = [
    url(r'^articles/$', views.articles),
    url(r'^page/(?P<slug>[\w-]+)/$', views.article)
]
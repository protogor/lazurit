from django.db import models
# Create your models here.


class Subscriber(models.Model):
    email = models.EmailField("Почта", unique=True)
    created = models.DateTimeField("Время создания", auto_now_add=True)

    class Meta:
        verbose_name = "Подписчик"
        verbose_name_plural = "Подписчики"
        ordering = ("id",)

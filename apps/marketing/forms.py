from django import forms


class SubscriberForm(forms.Form):
    email = forms.EmailField(label="E-mail", max_length=150, widget=forms.TextInput({"class": "lp_form-input"}))

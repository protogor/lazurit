from django.shortcuts import render
from .models import Subscriber
from .forms import SubscriberForm

from django.http import JsonResponse


def subscribe(request):
    if request.method == "POST":
        form = SubscriberForm(request.POST)
        if form.is_valid():
            Subscriber.objects.get_or_create(email=form.cleaned_data["email"])
            return JsonResponse({"status": "ok"})
        return JsonResponse({
                "status": "error", 
                "errors": form.errors
            })

    form = SubscriberForm()
    return render(request, "page/subscribe.html", {"form": form})

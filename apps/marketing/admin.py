from django.contrib import admin
from .models import Subscriber


class SubscriberAdmin(admin.ModelAdmin):
    list_display = ("id", "email", "created")
    list_display_links = ("id",)

    
admin.site.register(Subscriber, SubscriberAdmin)

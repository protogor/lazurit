from django.db import connection 
from collections import namedtuple


class Query:

    def __init__(self):
        self._cursor = connection.cursor()

        self._params = []
        self._query = ""

    @property
    def query(self,):
        return self._query 

    @property
    def query(self):
        return self._query 

    @property
    def params(self):
        return self._params

    @params.setter
    def params(self, value):
        self._params = value

    def namedtuplefetchall(self, cursor):
        desc = cursor.description
        nt_result = namedtuple('Result', [col[0] for col in desc])
        return [nt_result(*row) for row in cursor.fetchall()]

    def dictfetchall(self, cursor): 
        desc = cursor.description 
        return [
            dict(zip([col[0] for col in desc], row)) 
            for row in cursor.fetchall() 
        ]

    @property
    def result(self):
        self._cursor.execute(self.query, self.params)
        return self.dictfetchall(self._cursor)


class CategoryBrands(Query):
    
    def __init__(self, category_id):
        Query.__init__(self)
        self._query = "SELECT brand_id brands FROM cache WHERE category_id = %s GROUP BY brand_id"
        self._params.append(category_id)

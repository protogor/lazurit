import json

from django.db import connection, transaction


"""
CREATE TABLE cache (
    id              int  primary key not null,
    name            varchar (254),
    image           varchar (254),
    url             varchar (254),
    articul         varchar (120),
    model           varchar (120),
    
    
    brand_id        int,
    brand_name      varchar (120),
    category_id     int,
    category_name   varchar (120),
    category_ids     integer[],

    price           int,
    price_old       int,
    discount        smallint,

    params jsonb,

    new             boolean default false,
    action          boolean default false,

    active          boolean default true,
    deleted         boolean default false
);

CREATE INDEX id_index ON cache(id_index);
CREATE INDEX url_index ON cache(url_index);

CREATE INDEX category_id_index ON cache(category_id_index);
CREATE INDEX brand_id_index ON cache(brand_id_index);

CREATE INDEX params_index ON cache USING gin(params);
CREATE INDEX category_ids_index ON cache USING gin(category_ids);
"""

def cache_insert(products):   
    cursor = connection.cursor()

    products_insert = []
    for product in products:
        values = list(product.json().values())
        products_insert.append(values)
        if len(products_insert) > 1000:
            bulk_insert(cursor, products_insert)
            products_insert = []
        
    bulk_insert(cursor, products_insert)
    cursor.close()


    def bulk_insert(cursor, objects_list):
        with transaction.atomic():
            cursor.executemany("""
                INSERT INTO cache
                    (id, name, image, url, articul, model, brand_id, brand_name, category_id, category_ids, category_name, price, price_old, discount, params, new, action, active, deleted)
                VALUES
                    (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
                    """, objects_list)


def cache_set(product):
    """
    Таск обновляет или вставляет новый проуктв в кеш
    """
    cursor = connection.cursor()
    cursor.execute("""SELECT 1 FROM cache WHERE id = %s""", [product.id])

    if not cursor.fetchone():
        print("create")
        values = list(product.json().values())

        with transaction.atomic():
            cursor.execute("""
                INSERT INTO cache
                    (id, name, image, url, articul, model, brand_id, brand_name, category_id, category_ids, category_name, price, price_old, discount, params, new, action, active, deleted)
                VALUES
                    (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
                    """, values)
    else:
        print("update")
        values_json = product.json()
        product_id = int(values_json.pop("id"))
        values = list(values_json.values())
        values.append(product_id)
        with transaction.atomic():
            cursor.execute("""
                UPDATE cache SET
                    name=%s, image=%s, url=%s, articul=%s, model=%s, brand_id=%s, brand_name=%s, category_id=%s, category_ids=%s, category_name=%s, price=%s, price_old=%s, discount=%s, params=%s, new=%s, action=%s, active=%s, deleted=%s
                WHERE
                    id = %s 
                """, values)
    cursor.close()
        # insert to cache

import math 

from threading import Thread
from multiprocessing import Process
from abc import ABCMeta, abstractmethod 

from django.core.mail import EmailMessage
from django.conf import settings


class Command(metaclass=ABCMeta):
 
    @abstractmethod
    def execute(self):
         pass


class Broker(object):

    def __init__(self):
        self._commands = []

    def add(self, command):
        self._commands.append(command)

    def remove(self):
        self._commands.remove(command)        

    def run(self):
        for command in self._commands:
            command.execute()


class BrokerThread(Broker):

    def run(self):
        threads = []
        for command in self._commands:
            tr = Thread(target=command.__class__.execute, args=[command])
            tr.Daemon = True
            threads.append(tr)

        for thread in threads:
            thread.start()


class BrokerProcess(Broker):
    """
    For power tasks
    """
    def run(self):
        processes = []
        for command in self._commands:
            ps = Process(target=command.__class__.execute, args=[command])
            ps.Daemon = True
            processes.append(ps)

        for process in processes:
            process.start()


class Email(Command):

    def __init__(self, title="", template="", recipients=[], email_source=settings.DEFAULT_FROM_EMAIL):
        self._title = title
        self._template = template
        self._email_source = email_source
        self._recipients = recipients

    def get_title(self):
        return self._title

    def get_template(self):
        return self._template

    def get_recipients(self):
        return self._recipients

    def get_email_source(self):
        return self._email_source

    def execute(self):
        title = self.get_title()
        template = self.get_template()
        email_source = self.get_email_source()
        recipients = self.get_recipients()

        email = EmailMessage(title, template, email_source, recipients)
        email.content_subtype = 'html'
        email.send()


class Paginator:
    """
    Пагинатор
    """

    def __init__(self, page=1,  per_page=12, count=0):
        self._page = page 
        self._per_page = per_page 
        self._count = count
        self._page_count = math.ceil(self._count / float(self._per_page))

    def pages(self):
        return range(1, self._page_count + 1)

    def next(self):
        return min(self._page_count, self.select + 1)

    def prev(self):
        return max(1, self.select - 1)

    @property 
    def select(self):
        return self._page

    @property 
    def count(self):
        return self._page_count
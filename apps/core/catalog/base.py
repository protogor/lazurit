from django.db import connection 
from collections import namedtuple


SORT_TYPES = {
    "price": "price ASC",
    "-price": "price DESC",
    "name": "name ASC",
    "-name": "name DESC"
}

class Catalog:

    def __init__(self, page=1, per_page=12, category_id=None, brand_id=None, product_ids=[], filters={}, sort="", search=""):

        self._category_id = category_id
        self._brand_id = brand_id
        self._product_ids = product_ids
        self._search = search.upper()
        
        self._filter_brands = filters.get("brands", {})
        self._filter_params = filters.get("params", {})
        self._filter_max = filters.get("max")
        self._filter_min = filters.get("min")

        self._page = page
        self._per_page = per_page

        
        self._query_params = []

        self._order = ["active ASC", "price DESC"]

        self._select = "SELECT id, name, image, url, price, params"
        self._select_count = "SELECT COUNT(id)"

        self._from = "FROM cache"
        self._group_by = ""
        
        self._sort = sort

        self._cursor = connection.cursor()

    def get_select(self):
        return self._select

    def get_select_count(self):
        return self._select_count

    def get_from(self):
        return self._from

    def get_group_by(self):
        return self._group_by

    def get_where_general(self):
        
        conditions = [
            "active = true", 
            "price > 0"
        ]
        if self._category_id:
            conditions.append("%s = ANY(category_ids)")
            self._query_params.append(self._category_id)

        if self._product_ids:
            conditions.append("id IN %s")
            self._query_params.append(tuple(self._product_ids))

        if self._brand_id:
            conditions.append("brand_id = %s")
            self._query_params.append(self._brand_id)
    
        if self._filter_brands:
            conditions.append("brand_id IN %s")
            self._query_params.append(tuple(self._filter_brands))

        if self._filter_params:
            for key, value in self._filter_params.items():
                conditions.append("params->%s->>1 IN %s")
                self._query_params.append(str(key))
                self._query_params.append(tuple(value))

        if self._filter_max:
            conditions.append("price <= %s")
            self._query_params.append(int(self._filter_max))

        if self._filter_min:
            conditions.append("price >= %s")
            self._query_params.append(int(self._filter_min))

        if self._search:
            conditions.append("upper(name) LIKE upper(%s)")
            self._query_params.append("%" + self._search + "%")

            """
            conditions_like = []
            conditions_like.append("model LIKE %s")
            self._query_params.append("%" + self._search + "%")

            conditions_like.append("articul LIKE %s")
            self._query_params.append("%" + self._search + "%")

            conditions.append(" OR ".join(conditions_like))
            """

        conditions_query = " AND ".join(conditions)
        return "WHERE %s" % conditions_query

    def get_order(self):
        sorting = SORT_TYPES.get(self._sort, "name ASC")
        return "ORDER BY %s" % sorting

    def set_order(self, orders):
        self._order = []

    def get_limit(self):
        limit = self._per_page
        
        offset = (self._page - 1) * limit
        self._query_params.append(offset)
        self._query_params.append(limit)

        return "OFFSET %s LIMIT  %s"

    def get_query(self):
        query_elements = [
            self.get_select(),
            self.get_from(),
            self.get_where_general(),
            self.get_group_by(),
            self.get_order(),
            self.get_limit()
        ]
        query_elements = filter(lambda x: x, query_elements)
        query = " ".join(query_elements)
        return query

    def get_query_count(self):
        query_elements = [
            self.get_select_count(),
            self.get_from(),
            self.get_where_general(),
        ]
        query_elements = filter(lambda x: x, query_elements)
        query = " ".join(query_elements)
        return query

    def get_params(self):
        return self._query_params

    def namedtuplefetchall(self, cursor):
        desc = cursor.description
        nt_result = namedtuple('Result', [col[0] for col in desc])
        return [nt_result(*row) for row in cursor.fetchall()]

    def dictfetchall(self, cursor): 
        desc = cursor.description 
        return [
            dict(zip([col[0] for col in desc], row)) 
            for row in cursor.fetchall() 
        ]
 
    @property
    def products(self):        
        self._query_params = []
        #raise Exception(self._cursor.mogrify(self.get_query(), self.get_params()))
        self._cursor.execute(self.get_query(), self.get_params())
        #raise Exception(self.get_query())
        return self.dictfetchall(self._cursor)

    @property
    def count(self): 
        self._query_params = []
        #raise Exception(self._cursor.mogrify(self.get_query(), self.get_params()))
        self._cursor.execute(self.get_query_count(), self.get_params())
        #raise Exception(self.get_query())
        result = self.namedtuplefetchall(self._cursor)
        _count = int(result[0].count) if result else 0     
        return _count



class Query:

    def __init__(self):
        self._cursor = connection.cursor()

    def get_query(self):
        return self._query

    def get_params(self):
        return self._params

    def dictfetchall(self, cursor): 
        desc = cursor.description 
        return [
            dict(zip([col[0] for col in desc], row)) 
            for row in cursor.fetchall() 
        ]

    @property 
    def result(self):
        self._cursor.execute(self.get_query(), self.get_params())
        return self.dictfetchall(self._cursor)


class QueryFilterParams(Query):

    def __init__(self, *args, **kwargs):
        Query.__init__(self)
        self._query = """
                SELECT cpt.id, cpt.name, cpt.unit, jsonb_object_agg(DISTINCT cpv.value, cpy.id) params
                FROM cache 
                    JOIN catalog_product_characteristics as cpchar ON cpchar.product_id = cache.id 
                    JOIN catalog_property as cpy ON cpchar.property_id = cpy.id 
                    JOIN catalog_propertyname as cpt ON cpy.property_type_id = cpt.id
                    JOIN catalog_propertyvalue as cpv ON cpy.value_id = cpv.id
                WHERE %s
                GROUP BY cpt.id
        """
        query_where = ["cache.active = true", "cache.deleted = false", "cache.price > 0"]
        self._params = []
        if kwargs.get("category_id"):
            self._params.append(kwargs.get("category_id"))
            query_where.append("%s = ANY(cache.category_ids)")
        if kwargs.get("brand_id"):
            self._params.append(kwargs.get("brand_id"))
            query_where.append("cache.brand_id = %s")
        if kwargs.get("search"):
            conditions_like = []
            conditions_like.append("cache.name LIKE %s")
            self._params.append("%" + kwargs.get("search") + "%")

            conditions_like.append("cache.model LIKE %s")
            self._params.append("%" + kwargs.get("search") + "%")

            conditions_like.append("cache.articul LIKE %s")
            self._params.append("%" + kwargs.get("search") + "%")

            
            query_where.append("(%s)" % " OR ".join(conditions_like))
        query_where_str = " AND ".join(query_where)
        self._query = self._query % query_where_str

    @property
    def filters(self):
        result = self.result
        if result:
            return result
        return []


class QueryFilterMain(Query):

    def __init__(self, *args, **kwargs):
        Query.__init__(self)
        query_where = ["cp.active = true", "cp.deleted = false", "cp.price > 0"]
        self._params = []
        if kwargs.get("category_id"):
            self._params.append(kwargs.get("category_id"))
            query_where.append("%s = ANY(cp.category_ids)")
        if kwargs.get("brand_id"):
            self._params.append(kwargs.get("brand_id"))
            query_where.append("brand_id = %s")
        if kwargs.get("search"):
            conditions_like = []
            conditions_like.append("cp.name LIKE %s")
            self._params.append("%" + kwargs.get("search") + "%")

            conditions_like.append("cp.model LIKE %s")
            self._params.append("%" + kwargs.get("search") + "%")

            conditions_like.append("cp.articul LIKE %s")
            self._params.append("%" + kwargs.get("search") + "%")


            query_where.append("(%s)" % " OR ".join(conditions_like))
        query_where_str = " AND ".join(query_where)

        self._query = """
                SELECT MIN(cp.price) min, MAX(cp.price) max, jsonb_object_agg(DISTINCT cp.brand_id, cp.brand_name) brands
                FROM cache cp 
                WHERE %s
                GROUP BY active
        """ % query_where_str
    @property
    def filters(self):
        result = self.result
        if result:
            return result[0]
        return []


class QueryCount(Query):

    def __init__(self, *args, **kwargs):
        Query.__init__(self)
        query_where = ["cp.active = true", "cp.deleted = false", "cp.price > 0"]
        self._params = []
        if kwargs.get("category_id"):
            self._params.append(kwargs.get("category_id"))
            query_where.append("%s = ANY(cp.category_ids)")
        if kwargs.get("brand_id"):
            self._params.append(kwargs.get("brand_id"))
            query_where.append("brand_id = %s")
        if kwargs.get("search"):
            conditions_like = []
            conditions_like.append("cp.name LIKE %s")
            self._params.append("%" + kwargs.get("search") + "%")

            conditions_like.append("cp.model LIKE %s")
            self._params.append("%" + kwargs.get("search") + "%")

            conditions_like.append("cp.articul LIKE %s")
            self._params.append("%" + kwargs.get("search") + "%")

            
            query_where.append("(%s)" % " OR ".join(conditions_like))
        query_where_str = " AND ".join(query_where)
        
        self._query = """
                SELECT COUNT(id)
                FROM cache cp 
                WHERE %s
        """ % query_where_str
    @property
    def count(self):
        result = self.result
        if result:
            return result[0].get("count", 0)
        return []


class QueryProducts(Query):

    def __init__(self, *args, **kwargs):
        Query.__init__(self)
        query_where = ["cp.active = true", "cp.deleted = false", "cp.price > 0"]
        self._params = []
        if kwargs.get("product_ids"):
            self._params.append(tuple(kwargs.get("product_ids")))
            query_where.append("id IN %s")
    
        query_where_str = " AND ".join(query_where)
        
        self._query = """
                SELECT *
                FROM cache cp 
                WHERE %s
        """ % query_where_str
    @property
    def products(self):
        result = self.result
        if result:
            return result
        return []

from django.contrib import admin
from .models import Market, FeedBackCall, FeedBack, SendEmail, ReceiverEmail
# Register your models here.

class MarketAdmin(admin.ModelAdmin):
    list_display = ("id", "title", "address", "phones", "worktime", "coordinates", "subway", "order")
    list_editable = ("address", "phones", "worktime", "coordinates", "subway", "order")
    list_display_links = ("title",)


admin.site.register(Market, MarketAdmin)


class FeedbackAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "phone", "created")
    list_display_links = ("name",)


admin.site.register(FeedBackCall, FeedbackAdmin)


class FeedbackBigAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "email", "phone", "created")
    list_display_links = ("name",)


admin.site.register(FeedBack, FeedbackBigAdmin)


class ReceiverEmailAdmin(admin.TabularInline):
    model = ReceiverEmail
    extra = 1

class SendEmailAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "active", "created")
    list_display_links = ("name",)
    list_editable = ("active",)

    inlines = [ReceiverEmailAdmin]


admin.site.register(SendEmail, SendEmailAdmin)

from django.db import models

# Create your models here.

class Market(models.Model):

    title = models.CharField("Название", max_length=254, blank=True)
    address = models.CharField("Адрес", max_length=254, blank=True)
    departament = models.CharField("Район", max_length=200, blank=True)
    subway = models.CharField("Метро", max_length=200, blank=True)
    phones = models.CharField("Телефон", max_length=150, blank=True)
    worktime = models.CharField("Рабочее время", max_length=150, blank=True)
    coordinates = models.CharField("Координаты", max_length=150, blank=True)

    order = models.SmallIntegerField(default=1)

    class Meta:
        verbose_name="Магазин"
        verbose_name_plural="Магазины"
        ordering = ("order", "id")


class FeedBackCall(models.Model):
    name = models.CharField("Имя", max_length=150)
    phone = models.CharField("Телефон", max_length=40)
    text = models.TextField("Комментарий", blank=True)
    created = models.DateTimeField("Время создания", auto_now_add=True)

    class Meta:
        verbose_name = "Обратный звонок"
        verbose_name_plural = "Обратный звонок"
        ordering = ("-id",)



class FeedBack(models.Model):
    name = models.CharField("Имя", max_length=150)
    email = models.EmailField("Email", blank=True, null=True)
    phone = models.CharField("Телефон", max_length=40)
    city = models.CharField("Город", max_length=150)    
    text = models.TextField("Комментарий", blank=True)

    created = models.DateTimeField("Время создания", auto_now_add=True)


    class Meta:
        verbose_name = "Обратная связь"
        verbose_name_plural = "Обратная связь"
        ordering = ("-id",)


POST_ORDER = "o"
POST_FEED_BACK = "c"
POST_FORM = "f"

POST_TYPES = (
    (POST_ORDER, "Заказ"),
    (POST_FEED_BACK, "Обратный звонок"),
    (POST_FORM, "Форма обратной связи")
)

class SendEmail(models.Model):
    name = models.CharField("Название", max_length=150)
    type_mail = models.CharField("Тип", choices=POST_TYPES, max_length=1)

    active = models.BooleanField("Активный", default=True)
    created = models.DateTimeField("Дата создания", auto_now_add=True)  

    class Meta:
        verbose_name = "Отправка почты"
        verbose_name_plural = "Отправка почты"


class ReceiverEmail(models.Model):
    send_mail = models.ForeignKey("SendEmail", on_delete=models.CASCADE, related_name="emails")

    name = models.CharField("Получатель", max_length=150)
    email = models.EmailField("Email")
    active = models.BooleanField("Активный", default=True)
    created = models.DateTimeField("Дата создания", auto_now_add=True)  

    class Meta:
        verbose_name = "Получатель почты"
        verbose_name_plural = "Получатели почты"
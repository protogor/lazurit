from django import template

from django.contrib.humanize.templatetags.humanize import intcomma
from apps.catalog.models import Category, Product

register = template.Library()


@register.filter
def price_format(price):
    return intcomma(price).replace(",", " ")

@register.filter
def list_order_asc(values):
    values = list(values)
    try:
        values.sort(key=lambda x: int(x[0]))
    except:
        values.sort(key=lambda x: x[0])
    return values


@register.inclusion_tag("tags/general_menu.html")
def general_menu_big():
    categories = Category.objects.filter(parent__isnull=True, active=True)
    return {
        "categories": categories
    }


@register.inclusion_tag("tags/best_products.html", takes_context=True)
def best_products(context):
    brand = context.get("brand")
    products = Product.objects.filter(brand=brand, active=True, price__gt=0, best_product=True)[:6]
    return {
        "brand": brand,
        "products": products,
    }


@register.filter
def limit_l(llist, number):
    return list(llist)[:int(number)]


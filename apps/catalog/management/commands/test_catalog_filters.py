from django.core.management.base import BaseCommand, CommandError
from django.db import connection
from apps.core.catalog.base import QueryFilterParams, QueryFilterMain, QueryCount


class Command(BaseCommand):

    args = "brand_id"
    help =  "run process caching for brand_id param"

    def handle(self, *args, **options):
        c = QueryFilterParams(category_id=1)
        print(c.result)

        c = QueryFilterMain(category_id=1)
        print(c.result)

        c = QueryCount(category_id=1)
        print(c.result)

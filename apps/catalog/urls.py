from django.conf.urls import url, include

from . import views


urlpatterns = [
    url(r'^$', views.general_page),
    url(r'^delivery/$', views.delivery),
    url(r'^opt/$', views.optom),
    # catalog
    url(r'^product/(?P<slug>[\w-]+)/$', views.product, name="url_product"),    
    url(r'^shop/$', views.categories),
    url(r'^search/$', views.search),

    #url(r'^shop2/$', views.category),
    
    url(r'^(?P<category_slug>[\w-]+)/(?P<brand_slug>[\w-]+)/$', views.catalog),
    url(r'^(?P<category_slug>[\w-]+)/$', views.catalog),


]

from django.contrib import admin
from apps.catalog.models import *
from django_mptt_admin.admin import DjangoMpttAdmin
# Register your models here.


class ProductAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "active", "for_yamarket", "best_product", "group", "created")
    list_editable = ("group", "active", "for_yamarket", "best_product")
    list_display_links = ("name",)

admin.site.register(Product, ProductAdmin)


class CategoryAdmin(DjangoMpttAdmin):
    pass

admin.site.register(Category, CategoryAdmin)


class BrandBannerInline(admin.TabularInline):
    model = BrandBanner


class BrandAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "slug", "created")
    list_display_links = ("name",)
    inlines = [
        BrandBannerInline
    ]
    
admin.site.register(Brand, BrandAdmin)


class PropertyNameAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "order")
    list_editable = ("order", )
    list_display_links = ("name",)
    
admin.site.register(PropertyName, PropertyNameAdmin)


class PropertyAdmin(admin.ModelAdmin):
    list_display = ("id", "property_type", "value")
    list_display_links = ("property_type",)

    
admin.site.register(Property, PropertyAdmin)


class PropertyValueAdmin(admin.ModelAdmin):
    pass

    
admin.site.register(PropertyValue, PropertyValueAdmin)


class AdminGroupProduct(admin.ModelAdmin):
    list_display = ("id", "name")
    list_display_links = ("name",)
    
admin.site.register(GroupProduct, AdminGroupProduct)

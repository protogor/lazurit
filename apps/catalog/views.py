# -*- coding: utf-8 -*-
import re
import json 

from django.core.mail import send_mail
from django.shortcuts import render
from django.template.loader import render_to_string
from django.conf import settings
from django.db.models import F, Q

from django.template.loader import render_to_string
from django.http import JsonResponse, Http404, HttpResponsePermanentRedirect

from apps.catalog.models import Product, Category, Brand
from apps.articles.models import Article
from apps.core.catalog.base import Catalog, QueryFilterParams, QueryFilterMain, QueryCount
from apps.core.utils import Paginator
from apps.seo.models import PageStaticMeta

def general_page(request):
    brand_cookie = request.COOKIES.get("brand", "kyocera")
    brand_change = {
        "kyocera": "kyocera",
        "hp": "hp"
    }
    brand_select = brand_change.get(brand_cookie, "kyocera")
    try:
        brand = Brand.objects.get(slug=brand_select)
    except:
        brand = None

    return render(request, "page/general_page.html", {"brand": brand})


def delivery(request):
    return render(request, "page/delivery.html", {})


def optom(request):
    return render(request, "page/opt.html", {})


def auction(request):
    return render(request, "page/auction.html", {})


def about(request):
    return render(request, "page/about.html", {})


def categories(request):
    categories = Category.objects.filter(parent__isnull=True, active=True)
    return render(request, "catalog/categories.html", {"categories": categories})


def category(request):
    return render(request, "catalog/category.html", {})


def catalog(request, category_slug, brand_slug=None):
    if brand_slug is None:
        try:
            article = Article.objects.get(category__slug="static", slug=category_slug)
            return render(request, "articles/article.html", {"article": article})
        except:
            pass

    try:
        category = Category.objects.get(slug=category_slug)
    except Exception:
        raise Http404

    if brand_slug:
        try:
            category = category.get_descendants().get(slug=brand_slug)
        except Exception:
            raise Http404

    page = request.GET.get("page", 1)
    try:
        page = int(page)
    except Exception:
        page = 1
    catalog_view = request.COOKIES.get("catalog_view", "cell")
    if request.method == "POST":

        post = dict(request.POST)
        brands = json.loads(post["brands"][0])
        params = json.loads(post["params"][0])
        params_format = {}
        for param in params:
            params_format.setdefault(param["id"], []).append(param["value"])
        price_max = post["max"][0]
        price_min = post["min"][0]

        sort = request.COOKIES.get("sort", "price")

        filters = {
            "brands": brands,
            "params": params_format,
            "max": price_max,
            "min": price_min
        }
        try:
            page = int(post["page"][0])
        except:
            page = 1

        catalog = Catalog(page=page, category_id=category.id, filters=filters, sort=sort)            
        products = catalog.products
        count = catalog.count
               
        paginator = Paginator(page=page, count=count)
        products_template = render_to_string("catalog/catalog_products.html", {
            "products": products,
            "paginator": paginator,
            "catalog_view": catalog_view
        })

        return JsonResponse({"status": "ok", "template": products_template})

    sort = request.COOKIES.get("sort", "price")
    catalog = Catalog(page=page, category_id=category.id, sort=sort)    
    filters_params = QueryFilterParams(category_id=category.id)    
    filters_main = QueryFilterMain(category_id=category.id)   
    count = QueryCount(category_id=category.id)   
    paginator = Paginator(page=page, count=count.count)

        
    try:
        category_meta = PageStaticMeta.objects.get(category=category)
        meta = category_meta.get_meta()
    except:
        meta = None

    if meta is None:
        try:
            category_meta = PageStaticMeta.objects.get(type_meta="c")
            meta = category_meta.get_meta(category=category)
        except:
            meta = None

    return render(request, "catalog/catalog.html", {
            "meta": meta,
            "category": category,
            "products": catalog.products,
            "filters_params": filters_params.filters,
            "filters_main":filters_main.filters,
            "paginator": paginator,
            "catalog_view": catalog_view
        })
    

def product(request, slug):
    product = None
    try:
        product = Product.objects.get(slug=slug)
    except Exception:

        try:
            product = Product.objects.get(pmodel=slug)
            return HttpResponsePermanentRedirect("https://lazuritprint.ru%s" % product.url)
        except Exception:
            pass

    if not product:
        raise Http404
        
    try:
        product_meta = PageStaticMeta.objects.get(product=product)
        meta = product_meta.get_meta()
    except:
        meta = None

    if meta is None:
        try:
            product_meta = PageStaticMeta.objects.get(type_meta="p")
            meta = product_meta.get_meta(product=product)
        except:
            meta = None

    return render(request, "catalog/product.html", {
        "meta": meta,
        "product": product,
        "compatibility_text": product.compatibility_text
    })


# Create your views here.
def search(request):
    search = request.GET.get("search", "")
    page = request.GET.get("page", 1)
    try:
        page = int(page)
    except Exception:
        page = 1

    if request.method == "POST":

        post = dict(request.POST)
        brands = json.loads(post["brands"][0])
        params = json.loads(post["params"][0])
        params_format = {}
        for param in params:
            params_format.setdefault(param["id"], []).append(param["value"])
        price_max = post["max"][0]
        price_min = post["min"][0]

        sort = request.COOKIES.get("sort", "price")

        filters = {
            "brands": brands,
            "params": params_format,
            "max": price_max,
            "min": price_min
        }
        try:
            page = int(post["page"][0])
        except:
            page = 1

        product_search = Product.objects.prefetch_related('compatibility').filter(name__icontains=search)

        products_ids_comp = []
        if product_search.exclude(category_id__in=(5, 6, 7, 8)).exists():
            products_ids_comp = list(product_search.values_list('compatibility__id', flat=True))

        product_ids_search = list(product_search.values_list('id', flat=True))
        
        product_ids_search += products_ids_comp

        catalog = Catalog(page=page, product_ids=product_ids_search, filters=filters, sort=sort)            
        products = catalog.products
        count = catalog.count
               
        paginator = Paginator(page=page, count=count)
        products_template = render_to_string("catalog/catalog_products.html", {
            "products": products,
            "paginator": paginator
        })

        return JsonResponse({"status": "ok", "template": products_template})

    sort = request.COOKIES.get("sort", "price")
    
    product_search = Product.objects.prefetch_related('compatibility').filter(name__icontains=search)

    products_ids_comp = []
    if product_search.exclude(category_id__in=(5, 6, 7, 8)).exists():
        products_ids_comp = list(product_search.values_list('compatibility__id', flat=True))

    product_ids_search = list(product_search.values_list('id', flat=True))
    
    product_ids_search += products_ids_comp

    catalog = Catalog(page=page, product_ids=product_ids_search, sort=sort)   

    filters_params = QueryFilterParams(search=search)    
    filters_main = QueryFilterMain(search=search)   
   
    paginator = Paginator(page=page, count=catalog.count)
    return render(request, "catalog/catalog.html", {
            "search": search,
            "products": catalog.products,
            "filters_params": filters_params.filters,
            "filters_main":filters_main.filters,
            "paginator": paginator
        })
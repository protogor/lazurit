import datetime
import mptt
import os
import json

from collections import OrderedDict

from mptt.models import MPTTModel, TreeForeignKey

from django.db.models.signals import post_save, pre_save, pre_delete
from django.dispatch import receiver
from django.conf import settings
from django.db import models, connection
from django.core.urlresolvers import reverse
from django.conf import settings
from django.db.models import Q
from pytils.translit import slugify

from apps.core.cache.base import cache_set
from apps.core.query.base import CategoryBrands


class GroupProduct(models.Model):
    name = models.CharField("Название", max_length=100)
    created = models.DateTimeField("Время создания", auto_now_add=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Группа"
        verbose_name_plural = "Группы"


class Category(MPTTModel):
    parent = TreeForeignKey(
        'self', blank=True, null=True, verbose_name="Родитель", related_name='childrens')
    name = models.CharField("Название", max_length=100)
    slug = models.CharField("slug", max_length=256, db_index=True, blank=True)

    image = models.ImageField("Картинка", upload_to="images/categories/", blank=True, null=True)
    image_banner = models.ImageField("Баннер в каталоге", upload_to="images/banners/", blank=True, null=True)
    order = models.SmallIntegerField("Очередность вывода", default=10)
    active = models.BooleanField("Активный", default=True)
    modified = models.DateTimeField("Время создания", auto_now=True)
    created = models.DateTimeField("Время создания", auto_now_add=True)

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

    @property
    def url(self):
        if self.parent:
            return "/%s/%s/" % (self.parent.slug, self.slug)
        return "/%s/" % self.slug

    def brands(self):
        brand_ids = self.products.values_list("brand_id", flat=True)
        return Brand.objects.filter(id__in=brand_ids)

    def get_subcategories(self):
        return self.get_descendants().filter(active=True)

    class Meta:
        ordering = ("order", "name")
        verbose_name = "Категория"
        verbose_name_plural = "Категории"


CURRENCY_RU = "ru"
CURRENCY_EU = "eu"
CURRENCY_USA = "us"

CURRENCY_LIST = (
    (CURRENCY_RU, "Рубль"),
    (CURRENCY_RU, "ЕВРО"),
    (CURRENCY_RU, "Доллар"),
)


class Product(models.Model):
    group = models.ForeignKey("GroupProduct", related_name="similar", blank=True, null=True)
    category = models.ForeignKey("Category", related_name="products", blank=True, null=True)
    brand = models.ForeignKey('Brand', related_name="bproducts", blank=True, null=True)
    image_product = models.ImageField("Картинка", upload_to="images/products/")
    name = models.CharField("Название", max_length=256)
    slug = models.CharField("slug", max_length=256,  blank=True, db_index=True)
    pmodel = models.CharField("Модель", max_length=256)
    articul = models.CharField("Артикул", max_length=256, blank=True)
    description = models.TextField("Описание", blank=True)
    is_new = models.BooleanField("Новый", default=False)

    price = models.IntegerField("Цена", default=0)
    price_old = models.IntegerField("Старая цена", default=0)
    discount = models.SmallIntegerField("Скидка", default=0)
    currency = models.CharField("Валюта", choices=CURRENCY_LIST, default=CURRENCY_RU, max_length=2)
    quantity = models.IntegerField("Количество", default=10)
    characteristics = models.ManyToManyField("Property", verbose_name="Характеристики", blank=True, null=True)
    compatibility = models.ManyToManyField("self", verbose_name="Совместимость", blank=True, null=True)
    
    for_yamarket = models.BooleanField("Яндекс маркет", default=False)
    active = models.BooleanField("Активный", default=True)
    order = models.SmallIntegerField("Очередность вывода", default=10)
    best_product = models.BooleanField("На главную страницу", default=False)
    modified = models.DateTimeField('Время последнего изменения', auto_now=True)
    created = models.DateTimeField('Время создания', auto_now_add=True)



    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

    @property 
    def url(self):
        return reverse("url_product", args=(self.slug,))

    """
    def images(self):
        return self.images.filter(active=True)

    @property
    def image(self):
        images = self.images()
        if images:
            images_general = images.filter(general=True)
            if images_general:
                return images_general[0]
            return images[0]
        return None
    """

    def get_characteristics(self):
        return self.characteristics.filter(active=True).order_by("property_type__order")

    def get_characteristics_short(self):
        return self.characteristics.filter(active=True).order_by("property_type__order")[:2]

    def json(self):
        params = self.dict()
        params["params"] = json.dumps(dict(params["params"]), ensure_ascii=False)
        return params

    def dict(self):
        characteristics = []
        characteristics_product = self.get_characteristics()
        params = {}
        if characteristics_product:
            characteristics = list(characteristics_product.values_list("property_type__id", "value__value", "property_type__name" , "property_type__unit"))
            params = map(lambda param: (param[0], [param[2], param[1], param[3]]), characteristics)
        category_ids = self.category.get_ancestors(include_self=True).values_list("id", flat=True)
        return OrderedDict([
            ("id", self.id),
            ("name", self.name),
            ("image", self.image_product.url),
            ("url", self.url),
            ("articul", self.articul),
            ("model", self.pmodel),
            ("brand_id", self.brand_id),
            ("brand_name", self.brand.name),
            ("category_id", self.category_id),
            ("category_ids", list(category_ids)),
            ("category_name", self.category.name),
            ("price", self.price),
            ("price_old", self.price_old),
            ("discount", self.discount),
            ("params", dict(params)),
            ("new", self.is_new),
            ("action", False),
            ("active", self.active),
            ("delete", False)])
    
    def get_similar(self):
        from apps.core.catalog.base import QueryProducts

        if self.group:
            product_ids = Product.objects.filter(active=True, group=self.group, category_id=self.category_id) \
                                         .exclude(id=self.id).order_by('price').values_list("id")
            query = QueryProducts(product_ids=product_ids)
            return query.products

        return []

    def get_compatibility(self):
        from apps.core.catalog.base import QueryProducts

        product_ids = self.compatibility.filter(active=True, price__gt=0).values_list("id")
        if product_ids:
            query = QueryProducts(product_ids=product_ids)
            return query.products
        return []

    @property
    def compatibility_text(self):
        return ", ".join([item.get("model") for item in self.get_compatibility() if item.get("model")])

    def save(self, *args, **kwargs):        
        if not self.slug:
            self.slug = slugify(self.name.lower())
        return super(Product, self).save(*args, **kwargs)

    class Meta:
        verbose_name = "Продукт"
        verbose_name_plural = "Продукты"

@receiver(post_save, sender=Product)
def save_product(sender, instance, **kwargs):
    cache_set(instance)

@receiver(pre_delete, sender=Product)
def pre_delete_product(sender, instance, **kwargs):
    with connection.cursor() as cursor:
        cursor.execute("DELETE FROM cache WHERE id = %s", [instance.id])


class PropertyName(models.Model):
    name = models.CharField("Название", max_length=150)
    alias = models.CharField("Псевдоним", max_length=150, blank=True, null=True)
    unit = models.CharField("Единицы измерения", max_length=50, blank=True, null=True)

    order = models.SmallIntegerField("Очередность вывода", default=10) 

    def save(self, *args, **kwargs):
        if not self.alias:
            self.alias = slugify(self.name)
        return super(PropertyName, self).save(*args, **kwargs)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Название характеристики"
        verbose_name_plural = "Названия характеристики"
        ordering = ("order", "id")


class PropertyValue(models.Model):
    value = models.CharField("Значение", max_length=254)

    def __str__(self):
        return self.value

    class Meta:
        verbose_name = "Значение"
        verbose_name_plural = "Значения"


class Property(models.Model):
    property_type = models.ForeignKey("PropertyName")
    value = models.ForeignKey("PropertyValue", blank=True, null=True)
    active = models.BooleanField("Активный", default=True)

    def __str__(self):
        value = self.value.value if self.value else ""
        return "%s %s %s" % (self.property_type.name, value, self.property_type.unit)

    class Meta:
        verbose_name = "Характеристика"
        verbose_name_plural = "Характеристики"
        ordering = ("property_type__name",)


class Image(models.Model):
    product = models.ForeignKey("Product", related_name="images")
    title = models.CharField("Название", max_length=200, blank=True, null=True)
    image = models.ImageField("Картинка", upload_to="images/products/")
    order = models.SmallIntegerField("Очередность вывода", default=10)
    general = models.BooleanField("Главная картинка", default=False)
    active = models.BooleanField("Активный", default=True)
    created = models.DateTimeField("Время создания", auto_now_add=True)

    class Meta:
        verbose_name = u"Картинка"
        verbose_name_plural = u"Картинки"
        ordering = ("order", "id")


class Brand(models.Model):
    name = models.CharField("Название", max_length=100)
    slug = models.SlugField("slug", max_length=100)
    image = models.ImageField("Картинка", upload_to="images/brands/", blank=True, null=True)
    image_background = models.ImageField("Фон на главной", upload_to="images/brands/", blank=True, null=True)
    image_banner1 = models.ImageField("Баннер 1 на главной", upload_to="images/brands/", blank=True, null=True)
    image_banner2 = models.ImageField("Баннер 2 на главной", upload_to="images/brands/", blank=True, null=True)
    order = models.SmallIntegerField("Очередность вывода", default=10)
    created = models.DateTimeField('Время создания', auto_now_add=True)

    def __str__(self):
        return self.name

    def is_kyocera(self):
        return self.slug == "kyocera"

    def is_hp(self):
        return self.slug == "hp"

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)
        return super(Brand, self).save(*args, **kwargs)

    def url(self, category=None):
        if category:
            return "/%s/%s/" % (category.slug, self.slug)
        return "/%s/" % self.slug

    def get_banners(self):
        return self.banners.filter(active=True)

    class Meta:
        verbose_name = "Бренд"
        verbose_name_plural = "Бренды"
        ordering = ("order", "id")



class BrandBanner(models.Model):
    brand = models.ForeignKey(Brand, verbose_name="Бренд", related_name="banners")
    name = models.CharField("Название", max_length=100, blank=True)
    link = models.CharField("Ссылка", max_length=256, blank=True)
    image = models.ImageField("Картинка", upload_to="images/brands/", blank=True, null=True)
    active = models.BooleanField("Активный", default=True)
    created = models.DateTimeField("Время создания", auto_now_add=True)

from django.contrib import admin
from apps.seo.models import *
from apps.catalog.models import Product as PProduct

class PageStaticMetaAdmin(admin.ModelAdmin):
    list_display = ("id", "type_meta", "title", "meta_keys", "meta_description", "create")
    list_display_links = ("id",)
    list_editable = ("type_meta","title", "meta_keys", "meta_description")
    
admin.site.register(PageStaticMeta, PageStaticMetaAdmin)


class TextParamAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "slug", "param", "active")
    list_display_links = ("name",)
    list_editable = ("param", "active")

admin.site.register(TextParam, TextParamAdmin)


class RedirectAdmin(admin.ModelAdmin):
    list_display = ("id", "url_from", "url_to", "active", "create")
    list_display_links = ("id",)
    list_editable = ("url_from", "url_to", "active")

admin.site.register(Redirect, RedirectAdmin)

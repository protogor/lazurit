from django.shortcuts import render
from django.http import HttpResponse, Http404
from apps.seo.models import TextParam
from apps.catalog.models import Product, Category
from django.template import Context, Template


def robots(request):
    try:
        param = TextParam.objects.get(slug="robots")
    except Exception:
        param = None

    return HttpResponse(param.param, content_type="text/plain")
    

def sitemap(request):
    try:
        sitemap_p =  TextParam.objects.get(slug="sitemap", active=True)
    except Exception:
        raise Http404


    sitemap_t = Template(sitemap_p.param)
    products = Product.objects.filter(active=True, price__gt=0)
    categories = Category.objects.all()
   
    sitemap_r = sitemap_t.render(Context({
                            "products": products, 
                            "categories": categories}))
    return HttpResponse(sitemap_r, content_type="application/xml")



def yamarket(request):
    try:
        sitemap_p =  TextParam.objects.get(slug="yamarket", active=True)
    except Exception:
        raise Http404


    sitemap_t = Template(sitemap_p.param)
    products = Product.objects.filter(active=True, price__gt=0, for_yamarket=True)
    categories = Category.objects.all()
   
    sitemap_r = sitemap_t.render(Context({
                            "products": products, 
                            "categories": categories}))
    return HttpResponse(sitemap_r, content_type="application/xml")


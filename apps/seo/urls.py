from django.conf.urls import url
from apps.seo.views import robots, sitemap, yamarket

urlpatterns = [
    url(r'^robots.txt$', robots, name="url_robots"),
    url(r'^sitemap.xml$', sitemap, name="url_sitemap"),
    url(r'^yamarket.xml$', yamarket, name="url_yamarket")
]

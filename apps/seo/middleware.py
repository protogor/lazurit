from apps.seo.models import Redirect
from django.http import HttpResponsePermanentRedirect



class MiddlewareRedirect:

    def process_request(self, request):
        path = request.path
        try:
            redirect_item = Redirect.objects.get(url_from=path, active=True)
            return HttpResponsePermanentRedirect(redirect_item.url_to)
        except Exception:
            pass
from django.db import models
from django.template import Template, Context
from apps.catalog.models import Product, Category


META_PRODUCT = "p"
META_CATEGORY = "c"
META_PAGE = "a"

META_TYPES = (
    (META_PRODUCT, "Продукт"),
    (META_CATEGORY, "Категория"),
    (META_PAGE, "Страница"),
)

class PageStaticMeta(models.Model):
    type_meta = models.CharField("type", choices=META_TYPES, default=META_PAGE, max_length=2)

    slug_page = models.SlugField("Slug", max_length=254, db_index=True, unique=True, blank=True, null=True)
    product = models.ForeignKey(Product, blank=True, null=True, on_delete=models.SET_NULL)
    category = models.ForeignKey(Category, blank=True, null=True, on_delete=models.SET_NULL)

    title = models.CharField("title", max_length=256)
    meta_keys =  models.CharField("Keys", max_length=256)
    meta_description =  models.CharField("Description", max_length=256)

    
    active = models.BooleanField("Активно", default=True)
    create = models.DateTimeField("Время создания", auto_now_add=True)

    def get_meta(self, product=None, category=None):
        meta_fields = {
            "title": "",
            "keywords": "",
            "description": ""
        }
        if self.product:
            product_context = Context({
                "name": self.product.name,
                "model": self.product.pmodel,
                "sku": self.product.articul,
                "price": self.product.price,
                "category_name": self.category.name if self.category else "", 
                "brand_name": self.brand.name if self.brand else "" 
             })
            meta_fields["title"] = Template(self.title).render(product_context)
            meta_fields["keywords"] = Template(self.meta_keys).render(product_context) 
            meta_fields["description"] = Template(self.meta_description).render(product_context)

        elif self.category:
            category_context = Context({
                "name": self.category.name
             })
            meta_fields["title"] = Template(self.title).render(category_context)
            meta_fields["keywords"] = Template(self.meta_keys).render(category_context) 
            meta_fields["description"] = Template(self.meta_description).render(category_context)

        elif self.type_meta == META_PRODUCT and product:
            product_context = Context({
                "name": product.name,
                "model": product.pmodel,
                "sku": product.articul,
                "price": product.price,
                "category_name": product.category.name if product.category else "", 
                "brand_name": product.brand.name if product.brand else "" 
             })
            meta_fields["title"] = Template(self.title).render(product_context)
            meta_fields["keywords"] = Template(self.meta_keys).render(product_context) 
            meta_fields["description"] = Template(self.meta_description).render(product_context)

        elif self.type_meta == META_CATEGORY and category:
            category_context = Context({
                "name": category.name
             })
            meta_fields["title"] = Template(self.title).render(category_context)
            meta_fields["keywords"] = Template(self.meta_keys).render(category_context) 
            meta_fields["description"] = Template(self.meta_description).render(category_context)
        else:
            meta_fields["title"] = self.title
            meta_fields["keywords"] = self.meta_keys
            meta_fields["description"] = self.meta_description

        return meta_fields


    class Mete:
        verbose_name = "Мета текст"
        verbose_name_plural = "Мета тексты"


class TextParam(models.Model):
    name = models.CharField("Имя", max_length=150)
    slug = models.SlugField("Slug", max_length=150, db_index=True, unique=True) 
    param = models.TextField("Параметр", blank=True)
    active = models.BooleanField("Активно", default=True)

    class Mete:
        verbose_name = "Параметр"
        verbose_name_plural = "Параметры"


class Redirect(models.Model):
    url_from = models.CharField("Url from", max_length=256)
    url_to = models.CharField("Url from", max_length=256)

    active = models.BooleanField("Активно", default=True)
    create = models.DateTimeField("Время создания", auto_now_add=True)

    class Mete:
        verbose_name = "Редирект"
        verbose_name_plural = "Редирект"
from django.db import models

# Create your models here.

from apps.seo.models import PageStaticMeta, META_PAGE


def meta(request):

    try:
        path_clean = request.path.strip("/")
        if not path_clean:
            path_clean = "general-page"
        
        psm = PageStaticMeta.objects.get(slug_page=path_clean, type_meta=META_PAGE)
        return {"meta": psm.get_meta()}
    except Exception:
        return {"meta": {
            "title": "",
            "keywords": "",
            "description": ""
        }}

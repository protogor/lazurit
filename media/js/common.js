function setCookie(key, value, exp) {
            if (typeof exp === 'undefined') {
                exp =  1 * 24 * 60 * 60 * 1000;
            }
            var expires = new Date();
            expires.setTime(expires.getTime() + exp);
            document.cookie = key + '=' + value + '; path=/; expires=' + expires.toUTCString();

        }
        
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

var csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

function sameOrigin(url) {
    // test that a given url is a same-origin URL
    // url could be relative or scheme relative or absolute
    var host = document.location.host; // host + port
    var protocol = document.location.protocol;
    var sr_origin = '//' + host;
    var origin = protocol + sr_origin;
    // Allow absolute or scheme relative URLs to same origin
    return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
        (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
        // or any other URL that isn't scheme relative or absolute i.e relative.
        !(/^(\/\/|http:|https:).*/.test(url));
}

$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
            // Send the token to same-origin, relative URLs only.
            // Send the token only if the method warrants CSRF protection
            // Using the CSRFToken value acquired earlier
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

$(document).ready(function(){
    /* var saw_banner = getCookie('ad_banner');
    if (saw_banner == null) {
        var window_service = '<div style="width: 620px; text-align: center;" class="ad_banner"> \
        <div style="font-size: 19.5px; font-weight: bold;">С 15 июля снижение цен на основные позиции до 30% в связи со стабилизацией курса рубля. </div>\
        <br><br><a class="button lp_button_red" href="/media/files/LP_Kyocera_HP_Oki_Xerox_orig.xlsx">Скачать прайс ОПТ   </a> \
        </div>';
        setCookie('ad_banner', true, 3600 * 50 * 1000);
        $.fancybox.open(window_service);
    }

    $(".brands_slider ul").bxSlider({
        slideWidth: 220,
        minSlides: 6,
        maxSlides: 6,
        pager: false,
        auto: true
    });
    */
    $(document).on("click", "#new_recipient button", function(){
        try {
            ym(49310365,'reachGoal','otpravka_vsplyv_formy')
            console.log("otpravka_vsplyv_formy");
        } catch (err) {
            console.log(err);
        }
    });

    $(document).on("click", ".basket_add", function(){
        var product_id = $(this).data("product-id");
        var count = $(this).parent().find(".count_buy");
        $.post("/cart/add/", {"product_id": product_id, "count": count.val()}, function(data) {
            if (data.status == "ok") {
                $(".h_b_basket_count").html(data.count);
                $(".basket_price").html(data.sum);
                var click_add = 0;
                $("body, html").animate({scrollTop: 0}, "slow", function() {
                    if (click_add == 0) {
                        $(".h_b_basket").fadeOut("fast");
                        $(".h_b_basket").fadeIn("fast");    
                        click_add += 1;
                    }
                });
                try {
                    ym(49310365,'reachGoal','dobavit_v_korzinu')
                    console.log("dobavit_v_korzinu");
                } catch (err) {
                    console.log(err);
                }
            }
        })
    })

    $(".lp_tabs .lp_tabs_title__item").on("click", function(){
        $(".lp_tabs .lp_tabs_title__item").removeClass("active");
        $(".lp_tabs__content__items .lp_tabs__content__item").hide();
        $(".lp_tabs__content__items .lp_tabs__content__item").eq($(this).index()).show();
        $(this).addClass("active");
    })

    $(".decrease").on("click", function () {
        var count = $(this).parent().find(".count_buy");
        var val = parseInt(count.val());
        if (val > 1)
            count.val(val - 1);
    })

    $(".increase").on("click", function () {
        var count = $(this).parent().find(".count_buy");
        var val = parseInt(count.val());
        count.val(val + 1);
    })
})
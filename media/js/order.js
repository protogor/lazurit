$(document).ready(function(){
	$(".button-order").on("click", function(){
		$.post("", $(".lp_form_field-contacts").serialize(), function(data) {
			if (data.status == "error") {
				var errors = data.errors;
				$(".lp_form_field-contacts .lp_form-input").css("border", "1px solid rgba(0,0,0,.15)");
				$(".methods_pay__items").css("border-left", "1px solid #fff");
				for(var k in errors){
					if (k == "paids")
						$(".methods_pay__items").css("border-left", "1px solid red");
					else
						$("#id_" + k).css("border", "1px solid red");
				}

                $("body, html").animate({scrollTop: $('#form_contact-order').offset().top}, "slow");

			} else {
				try {
					ym(49310365,'reachGoal','zakaz_oformlen')
					console.log("zakaz_oformlen");
				} catch (err) {
					console.log(err);
				}
				$.fancybox.open('<div class="window window-order_ok"><div class="window-head"><div class="window-title">Заказ товара</div><div class="window-description">Заказ принят</div></div><div class="window-body">Ваш заказ №<b>' + data.order_id + '</b>. Наш менеджер свяжется с Вами в ближайшее время.</div></div>');
			}
		})

		return false;
	})
})
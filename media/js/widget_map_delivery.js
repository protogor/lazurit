var myPoints = [
             {coords: [55.6164, 37.4840], text: "Павильоны: Б 2/6-7-8 - склад-магазин"},
             {coords: [55.622128, 37.452484], text: "Строительный р-к 'Тополек' павильоны: В-85-86-87"}
        ];

function init2() {
    myMap = new ymaps.Map('map_block_2', {
            center: [55.6164,37.4840],
            zoom: 17,
            controls: ['zoomControl']
        });
        var myCollection = new ymaps.GeoObjectCollection();

        
   

        myCollection.add(new ymaps.Placemark(
            myPoints[0].coords, {
                balloonContentBody: myPoints[0].text
            },
            {
            iconImageHref: '/media/images/icon_address_point_3.png',
            iconLayout: 'default#image',
            iconImageSize: [30, 42],
            iconImageOffset: [-5, -38]
        }
        ));
    
    myMap.geoObjects.add(myCollection);

    myMap = new ymaps.Map('map_block_1', {
            center: [55.622128, 37.452484],
            zoom: 17,
            controls: ['zoomControl']
        });
        var  myCollection = new ymaps.GeoObjectCollection();

        
    

        myCollection.add(new ymaps.Placemark(
            myPoints[1].coords, {
                balloonContentBody: myPoints[1].text
            },
            {
            iconImageHref: '/media/images/icon_address_point_3.png',
            iconLayout: 'default#image',
            iconImageSize: [30, 42],
            iconImageOffset: [-5, -38]
        }
        ));


    

    myMap.geoObjects.add(myCollection);
}

ymaps.ready(init2);
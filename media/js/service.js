$(document).ready(function(){
    $(".psd_information_price_button button").on("click", function(){
        var service_id = $(this).data("service-id");
        var type = "service";
        $.post("/service/form/", {"type": type, "service_id": service_id}, function(data){
            $.fancybox(data.form_html)
        })
    })

    $(document).on("click", ".window_service_buy_block .form_base_button", function(){
    	var form = $(".window_service_buy_block .form_base").serialize();
    	$(".form_base").find(".form_error").remove();
    	$.post("/service/form/buy/", form, function(data){
            if(data.status == "ok")
                $.fancybox("<h2 style='text-align: center;'>Ваш заказ отправелн.<br>Наш менеджер свжяется с вами.</h2>");
            else
    		for(var k in data.errors) {
    			$("#id_" + k).parent().append("<i class='form_error'>"+data.errors[k][0]+"</div>");
    		}
    	})
    })

    $(".psd_information_image a").fancybox();
    $(".readmore_button").on("click", function(e){
        e.preventDefault();
        $(this).toggleClass("open");
        if($(this).hasClass("open")) 
            $(this).parent().find(".readmore_text").show();
        else
            $(this).parent().find(".readmore_text").hide();
        return false;
    })
})
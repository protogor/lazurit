$(document).ready(function(){
    if ($( window ).width() < 500) {
        $(".products_recommend .products__items").bxSlider({
            pager: false,
            minSlides: 2,  
            maxSlides: 2,  
            slideWidth: 270,
            slideMargin: 25,
            touchEnabled: true,
        });
    } else {
        $(".products_recommend .products__items").bxSlider({
            pager: false,
            minSlides: 4,  
            maxSlides: 4,  
            slideWidth: 270,
            slideMargin: 25,
            touchEnabled: false,
        });
    }

    $(".lp_tabs_title__items .lp_tabs_title__item").on("click", function() {
        $(".lp_tabs_title__items .lp_tabs_title__item").removeClass("active");
        $(this).addClass("active");
        $(".lp_tabs__content__items .lp_tabs__content__item").removeClass("active");
        $(".lp_tabs__content__items .lp_tabs__content__item").eq($(this).index()).addClass("active");
    })
})
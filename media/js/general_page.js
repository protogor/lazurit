$(document).ready(function(){

    $(".general_banner-block").bxSlider({
        pager: true,
        auto: true,
        touchEnabled: false,
    });

    $(".best_products__tabs .lp_tabs_title__item").on("click", function(){
    	var brand = $(this).text();
    	if (brand=="Hewlett-Packard"){
    		setCookie("brand", "hp");
    	} else {
    		setCookie("brand", "kyocera");
    	}
    	window.location.reload();
        return false;
    })

    $(".best_products_banner a img, .block_information .element__item img.button_change_brand").on("click", function(){
        var brand = getCookie("brand");
        if (brand == "hp")
            setCookie("brand", "kyocera");
        else
            setCookie("brand", "hp");
        window.location.reload();
        return false;
    })

})
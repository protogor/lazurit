$(document).ready(function(){
    $(".product_price_counter .decrease, .product_price_counter .increase").on("click", function(){
        var self = $(this);
        var count = $(this).parent().find(".count_buy").val();
        var item_id = $(this).parent().parent().parent().find(".product_delete").data("item-id");
        $.post("/cart/change/", {"count": count, "item_id": item_id}, function(data){
            if (data.status == "ok") {
                $(".h_b_basket_count").html(data.count);
                $(".basket_price").html(data.sum);
                $(".basket-price-value").html(data.sum);
                self.parent().parent().parent().find(".basket_item-sum-value").html(data.item_sum);
            }
        })
    })

    $(".product_delete").on("click", function(){
        var self = $(this);
        var item_id = $(this).data("item-id");
        $.post("/cart/delete/", {"item_id": item_id}, function(data){
            if (data.status == "ok") {
                $(".h_b_basket_count").html(data.count);
                $(".basket_price").html(data.sum);
                $(".basket-price-value").html(data.sum);
                self.parent().parent().remove();
            }
        })
    })

})
$(document).ready(function() {
    var cookie_sort = getCookie("sort");
    if (cookie_sort)
      $(".select-products_sort").val(cookie_sort)

    var catalog_view = getCookie("catalog_view");
    $(".paginator_view_catalog a").removeClass("active");
    if (catalog_view == "cell" || catalog_view == null)
      $(".paginator_view_catalog a.view_catalog-cell").addClass("active");
    else 
      $(".paginator_view_catalog a.view_catalog-rows").addClass("active");

	  var parent = $(".prices-values");
	  step = 1;
      var field_min2 = parent.find('.field_min');
      var field_max2 = parent.find('.field_max');
      var value_min = parseInt(field_min2.val());
      var value_max = parseInt(field_max2.val());
      parent.find(".prices_scroll").slider({
          range: true,
          min: value_min,
          max: value_max,
          step: step,
          values: [value_min , value_max],
          slide: function(event, ui) {
              field_min2.val(ui.values[0]);
              field_max2.val(ui.values[1]);
          }
      });

    $(".filter_change .catalog_filter__item__title").on("click", function(){
        $(this).parent().toggleClass("open");
    })

    $(".button-filter").on("click", function(){

        var brands = [];
        $(".catalog_filter__items .catalog_filter__item-brands input:checked").each(function(index) {
          brands.push($(this).val());
        })

        console.log(brands);

        var params = [];
        $(".catalog_filter__items .catalog_filter__item-params input:checked").each(function(index) {
          params.push({"id": $(this).data("param-id"), "value": $(this).val()});
        })
        console.log(params);

        var min = $(".field_min").val();
        var max = $(".field_max").val();
        var sort = $(".select-products_sort").val();
        $.post("", {"brands": JSON.stringify(brands), "params": JSON.stringify(params), "min": min, "max": max, "sort": sort}, function(data) {
          if (data.status == "ok")
            $(".catalog_products").html(data.template);
          var cookie_sort = getCookie("sort");
            if (cookie_sort)
              $(".select-products_sort").val(cookie_sort) 
          var catalog_view = getCookie("catalog_view");
          $(".paginator_view_catalog a").removeClass("active");
          if (catalog_view == "cell" || catalog_view == null)
            $(".paginator_view_catalog a.view_catalog-cell").addClass("active");
          else 
            $(".paginator_view_catalog a.view_catalog-rows").addClass("active");
        })
    })

    $(document).on("click", ".paginator_arrows a", function(){
      var brands = [];
        $(".catalog_filter__items .catalog_filter__item-brands input:checked").each(function(index) {
          brands.push($(this).val());
        })

        console.log(brands);

        var params = [];
        $(".catalog_filter__items .catalog_filter__item-params input:checked").each(function(index) {
          params.push({"id": $(this).data("param-id"), "value": $(this).val()});
        })
        console.log(params);

        var min = $(".field_min").val();
        var max = $(".field_max").val();
        var page = $(this).attr("href").split("=")[1];
        var sort = $(".select-products_sort").val();

        $.post("", {"brands": JSON.stringify(brands), "params": JSON.stringify(params), "min": min, "max": max, "page": page, "sort": sort}, function(data) {
          if (data.status == "ok")
            $(".catalog_products").html(data.template);
            var cookie_sort = getCookie("sort");
            if (cookie_sort)
              $(".select-products_sort").val(cookie_sort) 
            var catalog_view = getCookie("catalog_view");
            $(".paginator_view_catalog a").removeClass("active");
            if (catalog_view == "cell" || catalog_view == null)
              $(".paginator_view_catalog a.view_catalog-cell").addClass("active");
            else 
              $(".paginator_view_catalog a.view_catalog-rows").addClass("active");
        })
        return false;
    })

    $(document).on("change", ".select-products_sort", function(){
        var sort = $(this).val();
        setCookie("sort", sort);
        $(".button-filter").trigger("click");
        return false;
    })

    $(document).on("click", ".paginator_view_catalog a", function(){
        $(".paginator_view_catalog a").removeClass("active");
        $(this).addClass("active");
        var catalog_view = "cell";
        if ($(this).hasClass("view_catalog-rows"))
          catalog_view = "row";
        setCookie("catalog_view", catalog_view);
        $(".button-filter").trigger("click");
        return false;
    })


    
})
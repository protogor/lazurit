$(document).ready(function(){
	$(".button-contact").on("click", function(){
		if ($(this).hasClass("goal_form-contacts")) {
			var goal_contacts = true;
		} else if ($(this).hasClass("goal_form-subscribe")) {
			var goal_subscribe = true;
		}
		$.post("", $(".lp_form_field-contacts").serialize(), function(data) {
			console.log(data);
			if (data.status == "error") {
				var errors = data.errors;
				$(".lp_form_field-contacts .lp_form-input").css("border", "1px solid rgba(0,0,0,.15)");
				for(var k in errors){
					$("#id_" + k).css("border", "1px solid red");
				}
			} else {
				if (goal_contacts) {
					try {
						ym(49310365,'reachGoal','otpravka_formy_kontakty')
						console.log("otpravka_formy_kontakty");
					} catch (err) {
						console.log(err);
					}
				}
				if (goal_subscribe) {
					try {
						ym(49310365,'reachGoal','otpravka_formy_podpisatsya')
						console.log("otpravka_formy_podpisatsya");
					} catch (err) {
						console.log(err);
					}
				}
				$(".lp_form_field-contacts .lp_form-input").val("");
				$(".lp_form_field-contacts .lp_form-input").css("border", "1px solid green");
			}
		})
	})
})
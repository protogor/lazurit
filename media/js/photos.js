$(document).ready(function(){
    $(".photos_gallery__items a").fancybox();
    $(".photos_gallery_category__item").on("click", function(){
        $(".photos_gallery_category__item").removeClass("active");
        $(this).addClass("active");
        var category_type = $(this).data("category-type");
        $(".photo_type_" + category_type).show();
        if (category_type == "1") 
        	$(".photo_type_2").hide();
        else 
        	$(".photo_type_1").hide();
    })
})
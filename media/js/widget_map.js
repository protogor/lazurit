myPoints = [{coords: [55.6164, 37.4840], text: "Павильоны: Б 2/6-7-8 - склад-магазин"},
            {coords: [55.622128, 37.452484], text: "Строительный р-к 'Тополек' павильоны: В-85-86-87"}];
var myMap = null;
function init() {
    myMap = new ymaps.Map('map_block', {
            center: [55.6164,37.4840],
            zoom: 17,
            controls: ['zoomControl']
        });
        myCollection = new ymaps.GeoObjectCollection();

        
    for (var k in myPoints) {

        myCollection.add(new ymaps.Placemark(
            myPoints[k].coords, {
                balloonContentBody: myPoints[k].text
            },
            {
            iconImageHref: '/media/images/icon_address_point_3.png',
            iconLayout: 'default#image',
            iconImageSize: [30, 42],
            iconImageOffset: [-5, -38]
        }
        ));
    }

    myMap.geoObjects.add(myCollection);
}

ymaps.ready(init);
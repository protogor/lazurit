$(document).ready(function(){
    $(".pd_information_slider_items").bxSlider({
        pager: false,
        minSlides: 3,  
        maxSlides: 3,  
        slideWidth: 120,
        slideMargin: 10
    });
    $(".pd_big_image a").fancybox();
    $(document).on("click", ".pd_information_slider_items li", function(e){
        e.preventDefault();
        e.stopPropagation();
        var a = $(".pd_big_image").find("a");
        var img = $(".pd_big_image").find("img");

        var image_middle = $(this).data("image-middle");
        var image_big = $(this).data("image-big");
        a.attr("href", image_big);
        img.attr("src", image_middle);
        return false;
    })
    
     $(".readmore_button").on("click", function(e){
        e.preventDefault();
        $(this).toggleClass("open");
        if($(this).hasClass("open")) 
            $(this).parent().find(".readmore_text").show();
        else
            $(this).parent().find(".readmore_text").hide();
        return false;
    })
})
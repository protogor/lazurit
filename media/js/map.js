myPoints = {
            407: { coords: [59.9697, 30.4135], text: 'г. Санкт-Петербург Пискаревский проспект д. 25 оф. 208<br><br><br>Пн-Пт 9:00 - 18:00<br>Сб, Вс - выходной<br><br>Телефнон: 8 (812) 903-77-15'},
        };
var myMap = null;
function init() {
    myMap = new ymaps.Map('contacts-map', {
            center: [59.9697, 30.4135],
            zoom: 16,
            controls: ['zoomControl']
        });
    // Создаем коллекцию.
        myCollection = new ymaps.GeoObjectCollection();
    // Создаем массив с данными.
        

    // Заполняем коллекцию данными.
    for (var k in myPoints) {

        myCollection.add(new ymaps.Placemark(
            myPoints[k].coords, {
                balloonContentBody: myPoints[k].text
            }
        ));
    }

    // Добавляем коллекцию меток на карту.
    myMap.geoObjects.add(myCollection);

}

ymaps.ready(init);